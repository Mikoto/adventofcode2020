main = readFile "1" >>= putStrLn . show . f . g . map (\x -> read x :: Integer) . lines
  where f (x:_,  y:_)  | x + y == 2020 = x * y
        f (x:xs, _:ys)                 = f (x:xs, ys)
        f (_:xs, [])                   = f $ g xs
        g (x:xs) = (x:xs, xs)

main2 = readFile "1" >>= putStrLn . show . f . g . map (\x -> read x :: Integer) . lines
  where f (x:_,  y:_, z:_)  | x + y + z == 2020 = x * y * z
        f (x:xs, y:ys, _:zs)                    = f (x:xs, y:ys, zs)
        f (x:xs, _:y:ys, [])                    = f (x:xs, y:ys, ys)
        f (_:xs, _:[],   [])                    = f $ g xs
        g (x1:x2:xs) = (x1:x2:xs, x2:xs, xs)
