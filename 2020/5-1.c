#include <stdio.h>

int
main (void)
{
  const int hrange = 128;
  const int wrange = 8;
  int all[1000] = {0};

  freopen ("5", "r", stdin);
  do {
    int hstart = 0;
    int hsize = hrange;
    int wstart = 0;
    int wsize = wrange;

    for (int i = 0; i < 7; i++)
      {
	if (getchar () == 'B')
	  hstart = hstart + hsize / 2;
	hsize /= 2;
      }
    for (int i = 0; i < 3; i++)
      {
	if (getchar () == 'R')
	  wstart = wstart + wsize / 2;
	wsize /= 2;
      }
    all[hstart * wrange + wstart] = 1;
  } while (getchar() == '\n');
  for (int i = 1; i < 999; i++)
    if (all[i-1] && !all[i] && all[i + 1])
      printf ("%d\n", i);
}
