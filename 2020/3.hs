import Data.List

main = readFile "3" >>= putStrLn . show . product . tuple5map sum . unzip5 . map (\(i, elem) -> let f i = case elem !! (i `mod` length elem) of
                                                                                                            '#' -> 1
                                                                                                            _   -> 0
                                                                                                in
                                                                                                  tuple5 ((if i `mod` 2 == 0 then
                                                                                                             f (i `div` 2)
                                                                                                           else
                                                                                                             0):(map f [i, i * 3, i * 5, i * 7]))) . zip [0..] . lines
  where tuple5 [a, b, c, d, e] = (a, b, c, d, e)
        tuple5map f (a, b, c, d, e) = [f a, f b, f c, f d, f e]
