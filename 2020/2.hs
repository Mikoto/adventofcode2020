import Data.Char

count x = length . filter ((==) x)
main = readFile "2" >>= putStrLn . show . length . filter (parse 0 0) . lines
  where valid min max chr pass = let x = count chr pass
                                 in
                                   x >= min && x <= max
        parse min tmpnum ('-':xs)           = parse tmpnum 0 xs
        parse min tmpnum (' ':x:':':' ':xs) = valid min tmpnum x xs
        parse min tmpnum (x:xs)             = parse min (tmpnum * 10 + digitToInt x) xs

main2 = readFile "2" >>= putStrLn . show . length . filter (parse 0 0) . lines
  where valid min max chr pass = (if length pass > min - 1 then pass !! (min - 1) == chr else False) /= (if length pass > max - 1 then pass !! (max - 1) == chr else False)
        parse min tmpnum ('-':xs)           = parse tmpnum 0 xs
        parse min tmpnum (' ':x:':':' ':xs) = valid min tmpnum x xs
        parse min tmpnum (x:xs)             = parse min (tmpnum * 10 + digitToInt x) xs
