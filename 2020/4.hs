fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

main = readFile "4" >>= putStrLn . show . parse 0 []
  where
    parse :: Integer -> [String] -> String -> Integer
    parse i lst ('\n':xs) = parse (if all id $ map (\x -> x `elem` lst) fields then i + 1 else i) [] xs
    parse i lst (' ':xs) = parse i lst xs
    parse i lst [] = if all id $ map (\x -> x `elem` lst) fields then i + 1 else i
    parse i lst xs = str i lst "" xs
    str :: Integer -> [String] -> String -> String -> Integer
    str i lst buf (':':xs) = eat i (buf:lst) xs
    str i lst buf (x:xs) = str i lst (buf ++ [x]) xs
    eat :: Integer -> [String] -> String -> Integer
    eat i lst (' ':xs) = parse i lst xs
    eat i lst ('\n':xs) = parse i lst xs
    eat i lst (_:xs) = eat i lst xs
