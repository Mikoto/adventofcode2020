#include <stdio.h>

int
main (void)
{
  freopen ("6", "r", stdin);
  int out = 0;
  int c = 0;
  while (c != EOF) {
    int y[26] = {0};
    while (c != EOF) {
      c = getchar ();
      if (c == '\n')
	{
	  c = getchar ();
	  if (c == '\n')
	    break;
	}
      if (c != EOF) /* else intentionally not used */
	y[c - 'a'] = 1;
    }
    for (int i = 0; i < sizeof y / sizeof y[0]; i++)
      out += y[i];
  }
  printf ("%d\n", out);
  return 0;
}
