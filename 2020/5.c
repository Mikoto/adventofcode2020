#include <stdio.h>

int
main (void)
{
  const int hrange = 128;
  const int wrange = 8;
  int max = 0;
  freopen ("5", "r", stdin);
  do {
    int hstart = 0;
    int hsize = hrange;
    int wstart = 0;
    int wsize = wrange;
    int res;

    for (int i = 0; i < 7; i++)
      {
	if (getchar () == 'B')
	  hstart = hstart + hsize / 2;
	hsize /= 2;
      }
    for (int i = 0; i < 3; i++)
      {
	if (getchar () == 'R')
	  wstart = wstart + wsize / 2;
	wsize /= 2;
      }
    res = hstart * wrange + wstart;
    if (res > max)
      max = res;
  } while (getchar() == '\n');
  printf ("%d\n", max);
}
