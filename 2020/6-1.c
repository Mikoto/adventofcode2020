#include <stdio.h>
#include <string.h>

int
main (void)
{
  freopen ("6", "r", stdin);
  int out = 0;
  int c = 0;
  while (c != EOF) { /* group */
    unsigned char x[26];
    memset(x, 1, sizeof x);

    while (c != EOF) { /* person */
      unsigned char y[26] = {0};
      int tmp;

      do { /* character */
	c = getchar ();
	if (c == EOF || c == '\n')
	  break;
	y[c - 'a'] = 1;
      } while (1);

      for (int i = 0; i < sizeof y / sizeof y[0]; i++)
	x[i] &= y[i];

      tmp = getchar();
      if (tmp == '\n')
	break;
      else if (tmp == EOF)
	c = EOF;
      else
	ungetc(tmp, stdin);
    }

    for (int i = 0; i < sizeof x / sizeof x[0]; i++)
      out += x[i];
  }
  printf ("%d\n", out);
  return 0;
}
